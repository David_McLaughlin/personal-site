# Welcome to my personal blog site  

![site_home](/uploads/a8685fddd1fd58dd84c4dad68860b722/site_home.png)  

Give the live site a look at https://davidjmclaughlin.com/

## Goals
This isn't your run-of-the-mill SPA, this is an extremely lightweight, framework free, fully customizable static site. The main goals for this project are:
  * Freely hosted  
  * Minimal management, I don't want to fight a complex build/deployment system or framework. Most of the time I'll just be adding simple text & image blog posts, or making minor layout changes. So I don't want any impediments to simple changes or deploys.  
  * Fully functional without JS. I don't mind having some simple effects or enhancements in JS but for the purpose of this site JS should not be required
  * Mobile friendly  
  * Minimal bundle size/progressive

## Stack  
For zero cost hosting we need to look no further than [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/), it can host any static site at our own domain, and automatic deployments can be done through the free tier CI/CD. I've also added in CloudFlare in front of GitLab to for the free CDN they offer.

For tooling this project uses the [Jekyll](https://jekyllrb.com) static site generator, and [SASS](https://sass-lang.com/). Blog posts are written directly in markdown & html, see the [Jekyll Posts](https://jekyllrb.com/docs/posts/) page for more information.

## Project structure  
  * **_data** - this directory is for storing structured data, JSON, YAML, XML, etc, for use in the building of the site.  
  * **_includes** - reusable html template snippets are stored in this directory. These can be referenced using the standard [Jekyll Include tag](https://jekyllrb.com/docs/includes/).  
  * **_layouts** - these are the top level templates that page content is interpolated into.  
  * **_posts** - markdown files that get turned into blog posts, see the [Jekyll Posts](https://jekyllrb.com/docs/posts/) page for more information.  
  * **_sass** - this directory holds the SCSS files that get built into this site's CSS. Files prefixed with `component-` are SCSS files related to a snippet found in the `_includes` directory. i.e. `_sass/component.header.scss` has all the styles for the html found in the `_includes/header.html`. The `global-` prefix is for styles that apply to the site as a whole. Media breakpoints are kept in their own SCSS files, see `global-breakpoints.scss`, `global-small-phone.scss`, `global-medium-phone.scss`, `global-large-phone.scss`, and `global-desktop.scss`.  
  * **_schemas** - here are schema definitions for data in the `_data` directory. i.e. `_schemas/skills.schema.json` is a JSON schema file that defines the valid shape and values for data in the `_data/skills.yml` file. To get YAML validation in your editor some configuration is likely needed, see the section below for some guidance.  
  * **assets** - images, the CSS 'entry-point' (this is just the main SCSS file that references all the others).  

Pages are stored in the top-level directory, any `.html` file will become an accessible page.

## Using the _schema files  
In VSCode with the RedHat YAML extension you can get auto completion and linter errors by manually adding a mapping for the schema to the target data file. For the `skills.yml` file, open the RedHat YAML extension settings, navigate to the `Yaml: Schemas` section and click `Edit in settings.json`, from there you can create a mapping from a schema file to a data file, e.g.

```json
"yaml.schemas": {
    "./_schemas/skills.schema.json": ["/skills.yml", "/skills.yaml"],
}
```

This will give you intellisense and errors when editing the `skills.yml` file.

For more details see the RedHat [blog post about YAML schemas](https://developers.redhat.com/blog/2020/11/25/how-to-configure-yaml-schema-to-make-editing-files-easier/)
