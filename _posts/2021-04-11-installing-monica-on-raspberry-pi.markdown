---
layout: post
title: "Installing Monica on a Raspberry Pi"
date: 2021-04-11 00:00:00 -0500
categories: ['Personal Infrastructure', 'Self Hosted']
tags: ['pi', 'pi-zero', 'monica', 'self-host']
stage: staging
---

[Monica](https://www.monicahq.com/) is an opensource CRM made for recording and managing people in your personal life. Check the [features page](https://www.monicahq.com/features) to see what it can do. 