---
layout: post
title: "Building a Kubernetes cluster on Pi Zeros"
date: 2021-02-13 12:00:00 -0500
categories: ['Personal Infrastructure']
tags: ['pi', 'pi-zero', 'cluster', 'kubernetes', 'routing', 'linux', 'zero']
stage: staging
---

Pi Zeros can support USB host mode, so they can simulate devices like keyboards, storage, etc. Using some minimal configuration we can get our Pi Zeros running as Ethernet gadgets. See for minimal setup instructions https://gist.github.com/gbaman/975e2db164b3ca2b51ae11e45e8fd40a

To utilize the Pi Zeros we need a host machine, another Pi is a good choice. To keep things performant I went with a Pi 4B 2 GB, we shouldn't need too much memory, but the extra CPU performance of the Pi 4 will come in handy since it will be acting as a router for our 7 nodes.

## BoM
* 7 x Raspberry Pi Zero 1.3 (No need for Wifi, although I think there may be some minor HW improvements in the Zero W model)
  - 7 x Micro SD card
  - 7 x 8 inch micro USB cables
* 1 x Raspberry Pi 4B 2GB
  - 1 x Case
  - 1 x USB C Power supply
  - 1 x Micro SD card
* 1 x USB 3.0 10 port hub (3 ports power only)
* 1 x USB 120mm dual fan


## Software Setup

Pi Zero, Raspbian Lite, modify boot files (empty ssh file, config.txt, cmdline.txt), use as golden image, flash golden image x7.

Pi 4 host, ethernet, set static ip through dhcpcd, dnsmasq for DNS server for Pi Zeros, setup dnsmasq

* Static device mac, must be set in cmdline.txt, setting in `/etc/modprobe.d/g_ether.conf` only seems to affect device mac, so the host mac will continue to be random meaning its not easy to set static device names later. https://www.raspberrypi.org/forums/viewtopic.php?t=171791#p1100143

* Static network device names, requires editing cmdline.txt to get static host_mac, then create a new file /etc/systemd/network/1-usb.link where mappings can be specified. udev rule can be tested with `sudo udevadm test /sys/class/net/usb1`, output will show matching config file and actions taken, so you can see the rename. https://www.raspberrypi.org/forums/viewtopic.php?t=275822#p1671144. Also need to verify this https://www.raspberrypi.org/forums/viewtopic.php?t=191639#p1202818

* Still some issue when host pi is rebooted, all usb network adapters show with the same mac? unplugging/replugging the usb cable seems to fix it, so does rebooting individual nodes, guessing something is not being waited for on boot?